import subprocess


class DiskInfoBase(object):
    def get_disk_list(self):
        raise NotImplementedError()

    def get_disk_partition_list(self, disk):
        raise NotImplementedError()


class CommandPromptBase(object):
    COMMAND_LINE = None
    PROMPT = ">"

    def __init__(self):
        self.proc = subprocess.Popen(
            self.COMMAND_LINE,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        self._get_response()

    def _get_response(self):
        resp = ""
        while self.PROMPT not in resp and self.proc.poll() is None:
            resp += self.proc.stdout.read(1)
        return resp.rstrip(self.PROMPT)

    def execute(self, command):
        return command.parse(self._execute(command.render()))

    def _execute(self, command):
        self.proc.stdin.write(command + "\n")
        return self._get_response()
