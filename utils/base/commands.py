

class Command(object):
    line = None

    def __init__(self, *args):
        self.args = args

    def render(self):
        return self.line % self.args

    def parse(self, response):
        raise NotImplementedError()
