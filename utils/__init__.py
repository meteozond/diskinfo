from sys import platform
from .diskpart.api import DiskPartDiskInfo


PLATFORMS = dict(
    win32=DiskPartDiskInfo,
    win64=DiskPartDiskInfo
)


if platform not in PLATFORMS:
    raise Exception("Platform %s is not supported" % platform)


diskinfo = PLATFORMS[platform]()
