from utils.base.api import CommandPromptBase, DiskInfoBase
from .commands import GetDiskList, SelectDisk, GetPartitionList


class DiskPartDiskInfo(CommandPromptBase, DiskInfoBase):
    COMMAND_LINE = "diskpart"
    PROMPT = "DISKPART>"

    def get_disk_list(self):
        return self.execute(GetDiskList())

    def get_disk_partition_list(self, disk):
        self._select_disk(disk)
        return self._get_partition_list()

    def _select_disk(self, disk):
        return self.execute(SelectDisk(disk))

    def _get_partition_list(self):
        return self.execute(GetPartitionList())
