from utils.base.commands import Command


CLEANUP_CHARS = "\r", " "


def cleanup(text):
    for char in CLEANUP_CHARS:
        text = text.rstrip(char)
    return text


class TableResponseMixIn(object):
    TABLE_COLUMNS = []

    def parse(self, response):
        lines = [l for l in filter(None, map(cleanup, response.split("\n")))]
        return [dict(zip(self.TABLE_COLUMNS, filter(None, line.split("  "))))
                for line in lines[2:]]


class GetDiskList(TableResponseMixIn, Command):
    line = "list disk"
    TABLE_COLUMNS = ['id', 'status', 'size', 'free', 'is_dynamic', 'is_gpt']


class SelectDisk(Command):
    line = "select disk %s"

    def parse(self, response):
        return cleanup(response)


class GetPartitionList(TableResponseMixIn, Command):
    line = "list partition"
    TABLE_COLUMNS = ['id', 'type', 'size', 'offset']
