from utils import diskinfo
from sys import argv


if __name__ == "__main__":
    if len(argv) < 2:
        for disk in diskinfo.get_disk_list():
            print disk['id'], disk['size']
    else:
        for part in diskinfo.get_disk_partition_list(argv[1]):
            print part['id'], part['size']
